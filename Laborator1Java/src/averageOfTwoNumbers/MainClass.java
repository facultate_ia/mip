package averageOfTwoNumbers;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		int x = 0, y = 0;
		System.out.println("Enter a number:");
		Scanner keyboard = new Scanner(System.in);
		try {
			x = keyboard.nextInt();
			y = keyboard.nextInt();
			System.out.println(Utilities.average(x, y));
		}catch(InputMismatchException ex) {
			System.out.println("Write an integer number!");
		}catch(Exception ex) {
			System.out.println("An error ocurred!");
			ex.getMessage();
		}finally {
			keyboard.close();
		}
	}
	
}

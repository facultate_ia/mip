package figure;

public class Rectangle extends Figure {
	
	private float _width;
	private float _height;
	
	public Rectangle(float width,float height) {
		_width = width;
		_height = height;
	}
	public float GetArea() {
		return _width * _height;
	}
	
}

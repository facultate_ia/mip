package figure;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		float dim = 0;
		float result;
		boolean ok = false;
		Scanner scan = new Scanner(System.in);
		while(ok == false) {
			System.out.println("Insert square dimension: ");
			try {
				dim = scan.nextFloat();
				ok = true;
			} catch(InputMismatchException ex) {
				System.out.println("Please insert only numbers! ");
			} catch(Exception ex) {
				System.out.println("Exception occur! " + ex.getMessage());
			} finally {
				scan.close();
			}
		}
		Figure sq = new Square(dim, "red");
		result =  sq.GetArea();
		System.out.println(result);
	}

}

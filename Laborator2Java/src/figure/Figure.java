package figure;

public abstract class Figure {
	
	protected String _color;
	protected abstract float GetArea();
	
}

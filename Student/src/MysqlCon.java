import java.sql.*;

class MysqlCon {

    public static void main(String args[]) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con=DriverManager.getConnection("jdbc:sqlserver://ROBERTPC\\SQLEXPRESS;database=dbStudentBR","root","root");
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select * from dbStudentBR.dbo.Student");
            while (rs.next()) {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}  
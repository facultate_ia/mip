import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectURL {

    public static void main(String[] args) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://ROBERTPC\\SQLEXPRESS.database.windows.net:1433;integratedSecurity=true;"
                    + "database=dbStudentBR;"
                    + "encrypt=true;"
                    + "trustServerCertificate=false;"
                    + "loginTimeout=30;";
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            String selectSql = "SELECT * from Student";
            ResultSet resultSet = null;
            resultSet = statement.executeQuery("SELECT * FROM dbo.Student");

            // Print results from select statement
            while (resultSet.next()) {
                System.out.println(resultSet.getString(2) + " " + resultSet.getString(3));
            }

        } catch(SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getClass());
        }
    }
}
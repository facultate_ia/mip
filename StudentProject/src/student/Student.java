package student;

public class Student extends Person{
	
	private float _grade;
	
	public Student(String lastName, String firstName, float grade) {
		
		_lastName = lastName;
		_firstName = firstName;
		_grade = grade;
	}
	
	public float GetGrade() {
		
		return _grade;
	}
}

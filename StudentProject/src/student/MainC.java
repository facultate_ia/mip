package student;
//import java.util.InputMismatchException;
//import java.util.Scanner;
import java.awt.*;

public class MainC {
/*
	public static void main(String[] args) {
		float nota;
		Scanner scan = new Scanner(System.in);
		try {
			nota = scan.nextFloat();
			Person s = new Student("Duca", "Ileana", nota);
			System.out.println(s._firstName);
			//System.out.println();
		} catch (InputMismatchException ex) {
			System.out.println("");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		} finally {
			scan.close();
		}
	}

}
*/


	 public static void main (String args []) {
		 // Crearea ferestrei - un obiect de tip Frame
		 Frame f = new Frame("O fereastra");
		 f.setSize(500, 500);
		 // Setarea modului de dipunere a componentelor
		 f.setLayout (new FlowLayout());
		 // Crearea a doua butoane
		 Button b1 = new Button("OK");
		 Button b2 = new Button("Cancel");
		 // Adaugarea butoanelor
		 f.add(b1);
		 f.add(b2);
		 // Afisarea fereastrei
		 f.setVisible(true);
	 }
}